FROM alpine

LABEL MAINTAINER="João Petronio <contato@zerofill.com.br>" 
LABEL APP_VERSION=1.0.0

ENV NPM VERSION=8 ENVIROMENT=PROD

RUN sudo apt-get update && apt-get install -y git vim npm

WORKDIR /usr/share/myapp

RUN npm build

COPY ./requirements.txt requirements.txt

ADD ./files.tar.gz ./ 

ADD useradd petronio

USER petronio

EXPOSE 8080

VOLUME [ "/data" ]

ENTRYPOINT [ "ping" ]

CMD [ "localhost" ]